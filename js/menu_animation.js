
$(document).ready(function() {

  $('.catalog').click(function() {
      if ($('.order').css('display', 'none')) {
          $('.catalog').css('visibility', 'hidden');
          $('.order').fadeIn(800).css('display', 'block'); 
      }

  });

  $('[class*="container"]').click(function() {
      $(".order").css('display', 'none');
      $('.catalog').css('visibility', 'visible');
  });

});